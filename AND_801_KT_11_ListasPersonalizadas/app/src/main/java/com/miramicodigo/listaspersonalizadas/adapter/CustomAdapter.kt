package com.miramicodigo.listaspersonalizadas.adapter

import android.widget.TextView
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.BaseAdapter
import android.widget.ImageView
import com.miramicodigo.listaspersonalizadas.R
import java.util.*

class CustomAdapter(activity: Activity, data: ArrayList<Objects>) : BaseAdapter() {

    private val context: Context

    init {
        this.context = activity

    }

    override fun getCount(): Int {
        return 0
    }

    override fun getItem(position: Int): Any {
        return null!!
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var convertView = convertView
        val viewHolder: ViewHolder
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_lista, parent, false)
            viewHolder = ViewHolder(convertView)
            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as ViewHolder
        }





        return convertView
    }

    private inner class ViewHolder(view: View) {


        init {

        }
    }

}
