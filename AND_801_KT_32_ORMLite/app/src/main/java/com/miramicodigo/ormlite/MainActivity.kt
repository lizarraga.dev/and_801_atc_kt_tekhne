package com.miramicodigo.ormlite

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.Toast
import com.miramicodigo.ormlite.model.Persona
import com.miramicodigo.ormlite.model.PersonaDAO
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val personaDao = PersonaDAO()
    var id: Int? = 0
    var personaAux: Persona? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadList()

        btnAdicionar.setOnClickListener {


        }

        btnEliminar.setOnClickListener {



        }

        btnEliminarTodo.setOnClickListener {



        }
    }

    private fun loadList() {
        val allStudents: List<Persona>? = personaDao.queryForAll()
        val adapter = ArrayAdapter(this@MainActivity,
                android.R.layout.simple_list_item_1,
                allStudents)
        lvDatos.adapter = adapter

        lvDatos.setOnItemClickListener { adapterView, view, i, l ->



        }

        tvClickeado.text= ""
        etValores.setText("")
        personaAux = null
    }

}
