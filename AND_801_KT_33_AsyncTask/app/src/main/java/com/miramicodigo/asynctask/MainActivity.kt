package com.miramicodigo.asynctask

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.AsyncTask
import android.view.View
import java.net.HttpURLConnection
import java.net.URL
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


    }

    inner class AsyncTaskExample : AsyncTask<String, String, String>() {
        override fun onPreExecute() {
            super.onPreExecute()


        }

        override fun doInBackground(vararg p0: String?): String {

            return ""
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)



        }
    }

    fun convertToString(inStream: InputStream): String {
        var resultado: String = ""
        val isReader = InputStreamReader(inStream)
        var bReader = BufferedReader(isReader)
        var tempStr: String?

        try {
            while (true) {
                tempStr = bReader.readLine()
                if (tempStr == null) {
                    break
                }
                resultado += tempStr;
            }
        } catch(Ex: Exception) {
            println("Error en convertir a String ${Ex.printStackTrace()}")
        }
        return resultado
    }

}
